configure terminal
hostname Switch001
vtp mode transparent
vlan 1
name Default1
exit
vlan 100
name MGMT
exit
vlan 200
name SERVER
exit
vlan 300
name CLIENTS

Switch001(config)#interface range fastEthernet 0/1
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 100
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit

Switch001(config)#interface range fastEthernet 0/2
Switch001(config-if-range)#description Client001
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 300
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit

Switch001(config)#interface range fastEthernet 0/3
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 200
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit


Switch001(config)#interface range fastEthernet 0/6
Switch001(config-if-range)#description Client002
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 300
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit


Switch001(config)#interface range fastEthernet 0/7
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 200
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit


Switch001(config)#interface range fastEthernet 0/4
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 1
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit

Switch001(config)#interface range fastEthernet 0/8 - 12 
Switch001(config-if-range)#switchport mode access
Switch001(config-if-range)#switchport access vlan 1
Switch001(config-if-range)#spanning-tree portfast
Switch001(config-if-range)#exit

Switch001(config)#interface fastEthernet 0/6
Switch001(config-if)#switchport mode trunk


Switch001(config)#interface fastEthernet 0/1
Switch001(config-if)#shutdown
Switch001(config-if)#exit
Switch001(config)#interface fastEthernet 0/4
Switch001(config-if)#shutdown
Switch001(config-if)#exit

Switch001#copy running-config startup-config
Switch001#reload
